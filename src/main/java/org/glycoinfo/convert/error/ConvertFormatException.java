package org.glycoinfo.convert.error;

public class ConvertFormatException extends ConvertException {

	public ConvertFormatException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3843670778433622867L;

}
