package org.glycoinfo.convert;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

public abstract class GlyConvertWebService extends GlyConvertParent {
	private static final Log logger = LogFactory.getLog(GlyConvertWebService.class);

	protected String url;

	/**
	 * 
	 * dummy conversion using REST.  This should be overwritten.
	 * 
	 * @param fromSequence
	 * @return
	 * @throws ConvertException 
	 */
	public String convert(String fromSequence) throws ConvertException {
		RestTemplate restTemplate = new RestTemplate();
		Object[] output = null;
		ResponseEntity<String> response;
		String request = url + "?input=" + fromSequence;
		logger.debug(request);
	
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
        MediaType mediaType = new MediaType("application", "json", Charset.forName("UTF-8"));
        supportedMediaTypes.add(mediaType);
        MappingJackson2HttpMessageConverter jacksonConverter = new  MappingJackson2HttpMessageConverter();
        jacksonConverter.setSupportedMediaTypes(supportedMediaTypes);
        messageConverters.add(jacksonConverter);
        restTemplate.setMessageConverters(messageConverters);
        
		response = restTemplate.getForEntity(request, String.class);
		MediaType contentType = response.getHeaders().getContentType();
		HttpStatus statusCode = response.getStatusCode();
		return response.getBody();
	}
}