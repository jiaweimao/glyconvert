package org.glycoinfo.convert.glycoct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.glycoinfo.convert.iupac.IupacCondensedToGlycoctConverter;
import org.glycoinfo.convert.wurcs.WurcsToIupacCondensedConverter;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

/*
 */
@ConfigurationProperties(prefix = "GlycoctToWurcsToIupacConverterCheck")
@EnableAutoConfiguration
@ComponentScan
public class GlycoctToWurcsToIupacToGlycoCTCheckConverter extends GlyConvertParent {

	Log logger = LogFactory.getLog(GlycoctToWurcsToIupacToGlycoCTCheckConverter.class);
	
	@Override
	public String convert(String fromSequence) throws ConvertException {
		logger.debug("GlycoCT:\t>" + fromSequence + "<");
		GlycoctToWurcsConverter glycoctToWurcsConverter = new GlycoctToWurcsConverter();
		String wurcs = glycoctToWurcsConverter.convert(fromSequence);
		logger.debug("WURCS:\t>" + wurcs + "<");
		WurcsToIupacCondensedConverter wurcsToIupacCondensedConverter = new WurcsToIupacCondensedConverter();
		String iupac = wurcsToIupacCondensedConverter.convert(wurcs);
		logger.debug("IUPAC:\t>" + iupac + "<");
		IupacCondensedToGlycoctConverter iupacToGlycoctConverter = new IupacCondensedToGlycoctConverter();
		String glycoct = iupacToGlycoctConverter.convert(iupac);
		logger.debug("GlycoCT:\t>" + glycoct + "<");
		return glycoct;
	}

	@Override
	public String getFromFormat() {
		return GlyConvert.GLYCOCT;
	}

	@Override
	public String getToFormat() {
		return GlyConvert.GLYCOCT;
	}
}