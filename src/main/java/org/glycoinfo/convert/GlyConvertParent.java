package org.glycoinfo.convert;

import org.apache.commons.lang.StringUtils;
import org.glycoinfo.convert.error.ConvertException;

/**
 * @author aoki
 * 
 * Parents look after it's children for all of the basic necessities.  This class does simple validation on the input sequences.
 *
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
 *
 */
public abstract class GlyConvertParent implements GlyConvert {
	
	public String check(String fromSequence) throws ConvertException {
		if (StringUtils.isEmpty(fromSequence))
			throw new ConvertException("from sequence is empty >" + fromSequence + "<");
		return fromSequence;
	}
}
