package org.glycoinfo.convert.wurcs;

import org.glycoinfo.WURCSFramework.util.exchange.IUPAC.WURCSToIUPAC;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.stereotype.Component;

@Component

public class WurcsToIupacConverter extends GlyConvertParent {

	public String convert(String fromSequence) throws ConvertException {
		String str_IUPAC= check(fromSequence);
		
		WURCSToIUPAC a_converter = new WURCSToIUPAC();
        try {
			a_converter.start(fromSequence);
			str_IUPAC = a_converter.getExtendedIUPAC();
//			str_IUPAC = a_converter.getCondensedIUPAC();
		} catch (Exception e) {
			throw new ConvertException(e);
		}
		
		return str_IUPAC;
	}

	@Override
	public String getFromFormat() {
		return WURCS;
	}

	@Override
	public String getToFormat() {
		return IUPAC;
	}

}
