package org.glycoinfo.convert.wurcs;

import org.glycoinfo.WURCSFramework.util.exchange.IUPAC.WURCSToIUPAC;
import org.glycoinfo.convert.GlyConvertParent;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.stereotype.Component;

@Component

public class WurcsResourceCodeToIUPACConverter extends GlyConvertParent {

	public String convert(String fromSequence) throws ConvertException {
		String str_IUPAC= check(fromSequence);
		WURCSToIUPAC a_converter = new WURCSToIUPAC();
		
//		if(str_SkeletonCode.isEmpty()) return "";
//		if(str_SkeletonCode.contains("["))
//			str_SkeletonCode = str_SkeletonCode.replace("[", "");
//		if(str_SkeletonCode.contains("]")) 
//			str_SkeletonCode = str_SkeletonCode.replace("]", "");
		try {
			a_converter.start(fromSequence);
			str_IUPAC = a_converter.getExtendedIUPAC();
		} catch (Exception e) {
			throw new ConvertException(e);
		}
		
		return str_IUPAC;
	}

	@Override
	public String getFromFormat() {
		return WURCS;
	}

	@Override
	public String getToFormat() {
		return IUPAC;
	}

}
