package org.glycoinfo.convert.iupac;

import org.eurocarbdb.MolecularFramework.io.SugarImporter;
import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarExporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.io.iupac.SugarImporterIupacShortV1;
import org.eurocarbdb.MolecularFramework.io.namespace.GlycoVisitorToGlycoCTforIUPAC;
import org.eurocarbdb.MolecularFramework.sugar.Sugar;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.eurocarbdb.resourcesdb.Config;
import org.eurocarbdb.resourcesdb.io.MonosaccharideConverter;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.error.ConvertException;
import org.springframework.stereotype.Component;

@Component
public class IupacToGlycoctMFConverter implements GlyConvert {

	@Override
	public String convert(String fromSequence) throws ConvertException {
		SugarImporter t_objImporter = new SugarImporterIupacShortV1();
		Config t_objConf = new Config();

		MonosaccharideConverter t_objTrans = new MonosaccharideConverter(t_objConf);
		Sugar g1 = null;
		try {
			g1 = t_objImporter.parse(fromSequence);
		} catch (SugarImporterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SugarExporterGlycoCTCondensed exp = new SugarExporterGlycoCTCondensed();
		GlycoVisitorToGlycoCTforIUPAC t_objTo = new GlycoVisitorToGlycoCTforIUPAC(t_objTrans);
		try {
			t_objTo.start(g1);
			g1 = t_objTo.getNormalizedSugar();

			exp.start(g1);
		} catch (GlycoVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return exp.getHashCode();

	}

	@Override
	public String getFromFormat() {
		return GlyConvert.IUPAC;
	}

	@Override
	public String getToFormat() {
		return GlyConvert.GLYCOCT;
	}
}