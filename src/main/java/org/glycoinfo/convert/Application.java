package org.glycoinfo.convert;

import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.error.ConvertException;
import org.glycoinfo.convert.glycoct.GlycoctToWurcsConverter;
import org.glycoinfo.convert.wurcs.WurcsToIupacCondensedConverter;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

public class Application {

	private static final Log logger = LogFactory.getLog(Application.class);

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(Application.class, args);

		System.out.println(args.length);
		if (args.length < 1) {
			System.out.println("required parameters: <fromFormat> <toFormat> <sequence>");
			return;
		}
		String fromFormat = args[0];
		System.out.println("From:\t" + fromFormat);
		String toFormat = args[1];
		System.out.println("To:\t" + toFormat);
		String sequence = args[2];
		System.out.println("Sequence:\t>" + sequence + "<");
		String result = null;
		if (StringUtils.isBlank(fromFormat)) {
			GlyConvertDetect gcd = new GlyConvertDetect();
			try {
				result = gcd.convert(sequence, toFormat);
			} catch (ConvertException e) {
				System.err.println(e.getMessage());
				return;
			}
		}
		GlyConvert glyconvert = null;
		switch (fromFormat) {
		case GlyConvert.GLYCOCT:
			switch (toFormat) {
			case GlyConvert.WURCS:
				System.out.println(fromFormat + " to " + toFormat + " converter found");
				glyconvert = new GlycoctToWurcsConverter();
				break;

			default:
				break;
			}
			break;
		case GlyConvert.WURCS:
			switch (toFormat) {
			case GlyConvert.IUPAC:
				System.out.println(fromFormat + " to " + toFormat + " converter found");
				glyconvert = new WurcsToIupacCondensedConverter();
				break;

			default:
				break;
			}
			break;

		default:
			System.out.println("Trying to detect to " + toFormat + "");
			GlyConvertDetect gcd = new GlyConvertDetect();
			try {
				result = gcd.convert(sequence, toFormat);
			} catch (ConvertException e) {
				System.err.println(e.getMessage());
			}
			break;
		}
		try {
			result = glyconvert.convert(sequence);
		} catch (ConvertException e) {
			System.err.println(e.getMessage());
			return;
		}
		System.out.println("Result:\t>" + result + "<");
	}
}