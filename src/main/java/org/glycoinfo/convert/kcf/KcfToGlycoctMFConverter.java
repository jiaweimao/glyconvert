package org.glycoinfo.convert.kcf;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.eurocarbdb.MolecularFramework.io.SequenceFormat;
import org.eurocarbdb.MolecularFramework.io.SugarImporter;
import org.eurocarbdb.MolecularFramework.io.SugarImporterException;
import org.eurocarbdb.MolecularFramework.io.GlycoCT.SugarExporterGlycoCTCondensed;
import org.eurocarbdb.MolecularFramework.io.kcf.SugarImporterKCF;
import org.eurocarbdb.MolecularFramework.io.namespace.GlycoVisitorToGlycoCTforKCF;
import org.eurocarbdb.MolecularFramework.sugar.Sugar;
import org.eurocarbdb.MolecularFramework.util.visitor.GlycoVisitorException;
import org.eurocarbdb.resourcesdb.GlycanNamescheme;
import org.eurocarbdb.resourcesdb.io.MonosaccharideConversion;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.error.ConvertException;
import org.glycomedb.residuetranslator.ResidueTranslator;
import org.springframework.stereotype.Component;

@Component
public class KcfToGlycoctMFConverter implements GlyConvert {
	List<String> fromFormats = Arrays.asList(SequenceFormat.GLYCOCT_CONDENSED.getName());
	List<String> toFormats = Arrays.asList(SequenceFormat.KCF.getName());

	@Override
	public String convert(String fromSequence) throws ConvertException {

		String t_strCode = fromSequence;
		t_strCode = t_strCode.replaceAll("Glycan(.|\n)*NODE", "Glycan\nNODE");
		SugarImporter t_objImporter = new SugarImporterKCF();
		MonosaccharideConversion t_translator = null;
		try {
			t_translator = new ResidueTranslator();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sugar g1 = null;
		try {
			g1 = t_objImporter.parse(t_strCode);
		} catch (SugarImporterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// MonosaccharideConversion t_translator = new
		// MonosaccharideConverter(t_objConf);

		GlycoVisitorToGlycoCTforKCF t_objTo = new GlycoVisitorToGlycoCTforKCF(t_translator, GlycanNamescheme.KEGG);

		SugarExporterGlycoCTCondensed exp = new SugarExporterGlycoCTCondensed();
		try {
			t_objTo.start(g1);
		g1 = t_objTo.getNormalizedSugar();

		exp.start(g1);

		} catch (GlycoVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return exp.getHashCode().trim();
	}

	@Override
	public String getFromFormat() {
		return GlyConvert.KCF;
	}

	@Override
	public String getToFormat() {
		return GlyConvert.GLYCOCT;
	}
}