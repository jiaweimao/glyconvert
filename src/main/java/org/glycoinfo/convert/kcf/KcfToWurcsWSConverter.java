package org.glycoinfo.convert.kcf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.error.ConvertException;
import org.glycoinfo.convert.rings.GlyConvertWebServiceRings;
import org.springframework.stereotype.Component;

@Component
public class KcfToWurcsWSConverter extends GlyConvertWebServiceRings {
	private static final Log logger = LogFactory.getLog(KcfToWurcsWSConverter.class);
	
	@Override
	public String convert(String fromSequence) throws ConvertException {
		logger.debug("converting from:>" + fromSequence + "<");
		return super.convert(fromSequence);
	}

	@Override
	public String getFromFormat() {
		return RINGS_KCF;
	}

	@Override
	public String getToFormat() {
		return RINGS_WURCS;
	}
}