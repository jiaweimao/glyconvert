/**
 * 
 */
package org.glycoinfo.convert;

import static org.junit.Assert.assertEquals;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.error.ConvertException;
import org.glycoinfo.convert.glycoct.GlycoctToWurcsConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author aoki
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = VariousConverterTest.class)
@Configuration
public class VariousConverterTest {

	Log logger = LogFactory.getLog(VariousConverterTest.class);
	
	@Autowired
	protected GlyConvert converter;
	
//	"RES
//1b:x-dman-HEX-1:5 
//2b:a-dman-HEX-1:5 
//3b:a-dman-HEX-1:5 
//4b:a-dman-HEX-1:5 
//5s:anhydro 
//LIN 
//1:1o(1+1)2d 
//2:2o(6+1)3d 
//3:3o(2+1)4d 
//4:1d(2+1)5n 
//5:1o(5+1)5n"
	
	String str1="RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d";
	String str2="RES\n"
			+ "1b:a-lglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d";
	String str3="RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d";
	String str4="RES\n"
			+ "1b:a-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d";
	String str5="RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d";
	
	String kcf2="ENTRY         CT-1             Glycan\n"
			+ "NODE  2\n"
			+ "     1  GlcNAc   0   0\n"
			+ "     2  Gal   -8   0\n"
			+ "EDGE  1\n"
			+ "     1  2:b1     1:4\n"
			+ "///\n";
	/*
	 * 
	 * ENTRY         CT-1             Glycan
NODE  2
     1  a-L-GlcpNAc   0   0
     2  D-Galp   -8   0
EDGE  1
     1  2:b1     1:4
///
 * 
	 */
	String kcf3="ENTRY         CT-1             Glycan\n"
			+ "NODE  2\n"
			+ "     1  D-GlcpNAc   0   0\n"
			+ "     2  Gal   -8   0\n"
			+ "EDGE  1\n"
			+ "     1  2:b1     1:4\n"
			+ "///\n";
	     
	/*
	 * RES
1b:a-lglc-HEX-1:5
2s:n-acetyl
3b:b-dgal-HEX-1:5
LIN
1:1d(2+1)2n
2:1o(4+1)3d

ENTRY         CT-1             Glycan
NODE  2
     1  a-L-GlcpNAc   0   0
     2  D-Galp   -8   0
EDGE  1
     1  2:b1     1:4
///

	 */
	
	String kcf4="ENTRY         CT-1             Glycan\n"
			+ "NODE  2\n"
			+ "     1  GlcNAc   0   0\n"
			+ "     2  Gal   -8   0\n"
			+ "EDGE  1\n"
			+ "     1  2:b1     1:4\n"
			+ "///\n";
	
/*
 * RES
1b:x-dglc-HEX-1:5
2s:n-acetyl
3b:b-dgal-HEX-1:5
LIN
1:1d(2+1)2n
2:1o(4+1)3d

ENTRY         CT-1             Glycan
NODE  2
     1  D-GlcpNAc   0   0
     2  D-Galp   -8   0
EDGE  1
     1  2:b1     1:4
///

 * 
 */
	
	String kcf5="ENTRY         CT-1             Glycan\n"
			+ "NODE  2\n"
			+ "     1  D-GlcpNAc   0   0\n"
			+ "     2  Gal   -8   0\n"
			+ "EDGE  1\n"
			+ "     1  2:b1     1:4\n"
			+ "///\n";	
	
//	ENTRY         CT-1             Glycan\nNODE  2\n     1  D-GlcpNAc   0   0\n     2  Gal   -8   0\nEDGE  1\n     1  2:b1     1:4\n///
	
	String glycoct6="RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dglc-HEX-1:5\n"
			+ "4s:n-acetyl\n"
			+ "5s:(r)-lactate\n"
			+ "6b:b-dglc-HEX-1:5\n"
			+ "7s:n-acetyl\n"
			+ "8b:b-dglc-HEX-1:5\n"
			+ "9s:n-acetyl\n"
			+ "10s:(r)-lactate\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d\n"
			+ "3:3d(2+1)4n\n"
			+ "4:3o(3+1)5n\n"
			+ "5:3o(4+1)6d\n"
			+ "6:6d(2+1)7n\n"
			+ "7:6o(4+1)8d\n"
			+ "8:8d(2+1)9n\n"
			+ "9:8o(3+1)10n\n";
	
	String kcf6="ENTRY         CT-1             Glycan\n"
			+ "NODE  6\n"
			+ "     1  GlcNAc   -16   2\n"
			+ "     2  GlcNAc   -24   2\n"
			+ "     3  D-GlcpNAc   0   0\n"
			+ "     4  GlcNAc   -8   0\n"
			+ "     5  (r)-lactate   -16   -2\n"
			+ "     6  (r)-lactate   -32   2\n"
			+ "EDGE  5\n"
			+ "     1  4:b1     3:4\n"
			+ "     2  5:1     4:3\n"
			+ "     3  1:b1     4:4\n"
			+ "     4  2:b1     1:4\n"
			+ "     5  6:1     2:3\n"
			+ "///\n";
	
	String glcoct1 = "RES\n1b:b-dlyx-PEN-1:5\n";
	
	String glycoct2 = "RES\n"
			+ "1b:a-dglc-HEX-1:5\n"
			+ "2b:a-dglc-HEX-1:5\n"
			+ "3b:a-dglc-HEX-1:5\n"
			+ "4b:a-dglc-HEX-1:5\n"
			+ "5b:a-dglc-HEX-1:5\n"
			+ "6b:a-dglc-HEX-1:5\n"
			+ "7b:a-dglc-HEX-1:5\n"
			+ "8s:anhydro\n"
			+ "9s:anhydro\n"
			+ "LIN\n"
			+ "1:1o(4+1)2d\n"
			+ "2:2o(4+1)3d\n"
			+ "3:3o(4+1)4d\n"
			+ "4:4o(4+1)5d\n"
			+ "5:5o(4+1)6d\n"
			+ "6:6o(4+1)7d\n"
			+ "7:6d(3+1)8n\n"
			+ "8:6o(6+1)8n\n"
			+ "9:3d(3+1)9n\n"
			+ "10:3o(6+1)9n\n";
	
	String glycoct1kcf ="ENTRY         CT-1             Glycan\n"
			+ "NODE  1\n"
			+ "			1  b-D-Lyxp\n"
			+ "EDGE  0\n"
			+ "///\n";

	
	
	String G61646LF =	"RES\n"
			+ "1b:x-dglc-HEX-1:5\n"
			+ "2s:n-acetyl\n"
			+ "3b:b-dglc-HEX-1:5\n"
			+ "4s:n-acetyl\n"
			+ "5b:b-dman-HEX-1:5\n"
			+ "6b:a-dman-HEX-1:5\n"
			+ "7b:b-dglc-HEX-1:5\n"
			+ "8s:n-acetyl\n"
			+ "9b:b-dgal-HEX-1:5\n"
			+ "10b:b-dglc-HEX-1:5\n"
			+ "11s:n-acetyl\n"
			+ "12b:b-dgal-HEX-1:5\n"
			+ "13b:a-dman-HEX-1:5\n"
			+ "14b:b-dglc-HEX-1:5\n"
			+ "15s:n-acetyl\n"
			+ "16b:b-dgal-HEX-1:5\n"
			+ "17b:b-dglc-HEX-1:5\n"
			+ "18s:n-acetyl\n"
			+ "19b:b-dgal-HEX-1:5\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3d\n"
			+ "3:3d(2+1)4n\n"
			+ "4:3o(4+1)5d\n"
			+ "5:5o(3+1)6d\n"
			+ "6:6o(2+1)7d\n"
			+ "7:7d(2+1)8n\n"
			+ "8:7o(4+1)9d\n"
			+ "9:6o(4+1)10d\n"
			+ "10:10d(2+1)11n\n"
			+ "11:10o(4+1)12d\n"
			+ "12:5o(6+1)13d\n"
			+ "13:13o(2+1)14d\n"
			+ "14:14d(2+1)15n\n"
			+ "15:14o(4+1)16d\n"
			+ "16:13o(6+1)17d\n"
			+ "17:17d(2+1)18n\n"
			+ "18:17o(4+1)19d\n"
			+ "UND\n"
			+ "UND1:100.0:100.0\n"
			+ "ParentIDs:1|3|5|6|7|9|10|12|13|14|16|17|19\n"
			+ "SubtreeLinkageID1:o(3+2)d\n"
			+ "RES\n"
			+ "20b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n"
			+ "21s:n-acetyl\n"
			+ "LIN\n"
			+ "19:20d(5+1)21n\n"
			+ "UND2:100.0:100.0\n"
			+ "ParentIDs:1|3|5|6|7|9|10|12|13|14|16|17|19\n"
			+ "SubtreeLinkageID1:o(3+2)d\n"
			+ "RES\n"
			+ "22b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n"
			+ "23s:n-acetyl\n"
			+ "LIN\n"
			+ "20:22d(5+1)23n\n"
			+ "UND3:100.0:100.0\n"
			+ "ParentIDs:1|3|5|6|7|9|10|12|13|14|16|17|19\n"
			+ "SubtreeLinkageID1:o(3+2)d\n"
			+ "RES\n"
			+ "24b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n"
			+ "25s:n-acetyl\n"
			+ "LIN\n"
			+ "21:24d(5+1)25n\n"
			+ "UND4:100.0:100.0\n"
			+ "ParentIDs:1|3|5|6|7|9|10|12|13|14|16|17|19\n"
			+ "SubtreeLinkageID1:o(3+1)n\n"
			+ "RES\n"
			+ "26s:sulfate";
	
	
	
	String G00003VQ =	"RES\n"
			+ "1r:r1\n"
			+ "REP\n"
			+ "REP1:8o(4+1)2d=-1--1\n"
			+ "RES\n"
			+ "2b:b-dgal-HEX-1:5\n"
			+ "3s:n-acetyl\n"
			+ "4b:b-dglc-HEX-1:5|6:a\n"
			+ "5b:b-dgal-HEX-1:5\n"
			+ "6s:n-acetyl\n"
			+ "7b:a-dgal-HEX-1:5\n"
			+ "8b:b-dglc-HEX-1:5\n"
			+ "LIN\n"
			+ "1:2d(2+1)3n\n"
			+ "2:2o(3+1)4d\n"
			+ "3:4o(4+1)5d\n"
			+ "4:5d(2+1)6n\n"
			+ "5:5o(4+1)7d\n"
			+ "6:7o(3+1)8d";

	String G00369GT =	"RES\n"
			+ "1b:b-dglc-HEX-1:5\n"
			+ "2s:amino\n"
			+ "3r:r1\n"
			+ "4b:b-dglc-HEX-1:5\n"
			+ "5s:amino\n"
			+ "LIN\n"
			+ "1:1d(2+1)2n\n"
			+ "2:1o(4+1)3n\n"
			+ "3:3n(4+1)4d\n"
			+ "4:4d(2+1)5n\n"
			+ "REP\n"
			+ "REP1:6o(4+1)6d=1-8\n"
			+ "RES\n"
			+ "6b:b-dglc-HEX-1:5\n"
			+ "7s:amino\n"
			+ "LIN\n"
			+ "5:6d(2+1)7n\n";
	
//	String glycoct1 = "RES\n1b:o-lrib-PEN-0:0|1:keto|5:keto";
//
//	String wurcs1 = "WURCS=2.0/1,0/[o222o]";
	
	String glycoct3
	= "RES\n"
	+ "1b:b-dglc-HEX-1:5\n"
	+ "2s:methyl\n"
	+ "3s:sulfate\n"
	+ "4b:b-dgro-HEX-1:5|2:d|3:d|4:d|6:a\n"
	+ "5b:a-dglc-HEX-1:5\n"
	+ "6b:o-dman-HEX-0:0|1:aldi\n"
	+ "7b:x-dgro-dgal-NON-2:6|2:keto|1:a|3:d\n"
	+ "8s:n-acetyl\n"
	+ "9b:b-dara-HEX-2:5|2:keto\n"
	+ "10b:o-dgro-TRI-0:0|1:aldi\n"
	+ "11b:b-dxyl-HEX-1:5|1:keto|4:keto|6:d\n"
	+ "12b:b-drib-HEX-1:5|2:d|6:d\n"
	+ "13b:b-dglc-HEX-1:5\n"
	+ "14s:n-acetyl\n"
	+ "15s:phospho-ethanolamine\n"
	+ "16b:b-HEX-x:x\n"
	+ "17b:b-SUG-x:x\n"
	+ "18b:o-SUG-0:0\n"
	+ "19b:a-dery-HEX-1:5|2,3:enx\n"
	+ "20b:a-dman-OCT-x:x|1:a|2:keto|3:d\n"
	+ "LIN\n"
	+ "1:1o(2+1)2n\n"
	+ "2:1o(3+1)3n\n"
	+ "3:1o(4+1)4d\n"
	+ "4:4o(6+1)5d\n"
	+ "5:5o(4+1)6d\n"
	+ "6:6o(6+2)7d\n"
	+ "7:7d(5+1)8n\n"
	+ "8:7o(8+2)9d\n"
	+ "9:9o(4+1)10d\n"
	+ "10:10o(3+1)11d\n"
	+ "11:11o(3+1)12d\n"
	+ "12:12o(4+1)13d\n"
	+ "13:13d(2+1)14n\n"
	+ "14:13o(6+1)15n\n"
	+ "15:15n(1+1)16o\n"
	+ "16:16o(4+1)17d\n"
	+ "17:17o(-1+1)18d\n"
	+ "18:18o(-1+1)19d\n"
	+ "19:19o(4+2)20d\n"
;


	
	String wurcs3 = "WURCS=2.0/15,14/[12122h+1:b|1,5|2*OC|3*OSO/3=O/3=O][1ddd2a+1:b|1,5][22122h+1:a|1,5][h1122h][aXd21122h+2:x|2,6|5*NCC/3=O][h1122h+2:b|2,5][h2h][121k2m+1:b|1,5][1d222m+1:b|1,5][12122h+1:b|1,5|2*NCC/3=O][QXXXXh+?:b|?,?][Q<cQ>+?:b|?,?][o<nX>h][2ZZ22h+1:a|1,5][aQd1122h+?:a|?,?]1+4,2+1|2+6,3+1|3+4,4+1|4+6,5+2|5+8,6+2|6+4,7+1|7+3,8+1|8+3,9+1|10+1,9+4|10+6-2,11+1-1*ONCCOP^XO*/7O/7=O|11+4,12+1|12+?,13+1|13+?,14+1|14+4,15+2";

	String wurcs4 = "WURCS=2.0/5,5,4/[x1122h-1x_1-5][21122h-1a_1-5][22112h-1a_1-5][22122h-1a_1-5][12112h-1b_1-5_4,6*OC^XO*/3CO/6=O/3C]/1-2-3-4-5/a2-b1_b2-c1_b3-d1_d4-e1";
	/**
	 * Test method for {@link org.glycoinfo.conversion.kcf.GlycoctToconverter#convert()}.
	 */
	@Test(expected=ConvertException.class)
	public void testBlank() throws Exception {
		check(null, "");
	}
	
	public void test1() throws Exception {

		String result = converter.convert(str2);
		assertEquals(kcf2, result);
	}
	
	public void test2() throws Exception {
		String result = converter.convert(str2);
		assertEquals(kcf2, result);
	}
	
	public void test3() throws Exception {
		String result = converter.convert(str3);
		assertEquals(kcf3, result);
	}
	
	public void test4() throws Exception {
		String result = converter.convert(str4);
		assertEquals(kcf4, result);
	}
	
	public void test5() throws Exception {
		String result = converter.convert(str5);
		assertEquals(kcf5, result);
	}

	public void test6() throws Exception {
		String result = converter.convert(glycoct6);
		assertEquals(kcf6, result);
	}

	public void test7() throws Exception {
		check(glycoct1kcf, glcoct1);
	}

	@Test
	public void testrep() throws Exception {
		String glycoctrep = "RES\n1r:r1\nREP\nREP1:2o(4+1)2d=-1--1\nRES\n2b:b-dman-HEX-1:5";
		String wurcs = "WURCS=2.0/1,1,1/[a1122h-1b_1-5]/1/a1-a4~n";
		check(wurcs, glycoctrep);
	}

	String glycoct1 = "RES\n1b:o-lrib-PEN-0:0|1:keto|5:keto";
	String wurcs1 = "WURCS=2.0/1,1,0/[o222o]/1/";

	@Test
	public void testct1() throws Exception {
		check(wurcs1, glycoct1);
	}

	@Test
	public void testG61646LF() throws Exception {
		String wurcs = "WURCS=2.0/6,16,16/[a2122h-1x_1-5_2*NCC/3=O][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a2112h-1b_1-5][Aad21122h-2a_2-6_5*NCC/3=O]/1-2-3-4-2-5-2-5-4-2-5-2-5-6-6-6/a4-b1_b4-c1_c3-d1_c6-i1_d2-e1_d4-g1_e4-f1_g4-h1_i2-j1_i6-l1_j4-k1_l4-m1_n2-a3|b3|c3|d3|e3|f3|g3|h3|i3|j3|k3|l3|m3}_o2-a3|b3|c3|d3|e3|f3|g3|h3|i3|j3|k3|l3|m3}_p2-a3|b3|c3|d3|e3|f3|g3|h3|i3|j3|k3|l3|m3}_a3|b3|c3|d3|e3|f3|g3|h3|i3|j3|k3|l3|m3}*OSO/3=O/3=O";
		check(wurcs, G61646LF);
	}

	@Test
	public void testG00003VQ() throws Exception {
		String wurcs = "WURCS=2.0/4,5,5/[a2112h-1b_1-5_2*NCC/3=O][a2122A-1b_1-5][a2112h-1a_1-5][a2122h-1b_1-5]/1-2-1-3-4/a3-b1_b4-c1_c4-d1_d3-e1_a1-e4~n";
		check(wurcs, G00003VQ);
	}

	@Test
	public void testG00369GT() throws Exception {
		String wurcs = "WURCS=2.0/1,3,3/[a2122h-1b_1-5_2*N]/1-1-1/a4-b1_b4-c1_b1-b4~1:8";
		check(wurcs, G00369GT);
	}

	@Test
	public void testG00092LH() throws Exception {
		String glycoct = "RES\n"
				+ "1b:x-dglc-HEX-1:5\n"
				+ "2b:x-lgal-HEX-1:5|6:d\n"
				+ "3s:n-acetyl\n"
				+ "4b:b-dglc-HEX-1:5\n"
				+ "5s:n-acetyl\n"
				+ "6b:b-dman-HEX-1:5\n"
				+ "7b:a-dman-HEX-1:5\n"
				+ "8b:b-dglc-HEX-1:5\n"
				+ "9s:n-acetyl\n"
				+ "10b:x-lara-PEN-1:4\n"
				+ "11b:a-dman-HEX-1:5\n"
				+ "12b:x-lara-PEN-1:4\n"
				+ "13b:x-dxyl-PEN-1:5\n"
				+ "LIN\n"
				+ "1:1o(-1+1)2d\n"
				+ "2:1d(2+1)3n\n"
				+ "3:1o(4+1)4d\n"
				+ "4:4d(2+1)5n\n"
				+ "5:4o(4+1)6d\n"
				+ "6:6o(-1+1)7d\n"
				+ "7:7o(-1+1)8d\n"
				+ "8:8d(2+1)9n\n"
				+ "9:7o(-1+1)10d\n"
				+ "10:6o(-1+1)11d\n"
				+ "11:11o(-1+1)12d\n"
				+ "12:6o(-1+1)13d\n";
		String wurcs = 
				"WURCS=2.0/7,10,9/[a2122h-1x_1-5_2*NCC/3=O][a1221m-1x_1-5][a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5][a211h-1x_1-4][a212h-1x_1-5]/1-2-3-4-5-3-6-5-6-7/a4-c1_c4-d1_a?-b1_d?-e1_d?-h1_d?-j1_e?-f1_e?-g1_h?-i1";
		
		check(wurcs, glycoct);
	}
	
//	G80502ZO
	@Test
	public void testG80502ZO() {
		String glycoct = "RES\n"
				+ "1b:x-dara-PEN-1:5|1:a|5:a\n"
				+ "2b:b-dglc-HEX-1:5\n"
				+ "3s:n-acetyl\n"
				+ "4b:b-dglc-HEX-1:5|6:a\n"
				+ "LIN\n"
				+ "1:1o(3+1)2d\n"
				+ "2:2d(2+1)3n\n"
				+ "3:2o(3+1)4d\n";
		String wurcs = 
				"WURCS=2.0/3,3,2/[a122a_1-5][12122h-1b_1-5_2*NCC/3=O][12122a-1b_1-5]/1-2-3/a3-b1_b3-c1";
		
		try {
			check(wurcs, glycoct);
		} catch (ConvertException e) {
			logger.debug(e.getMessage());
		}
	}
	
	// G39378FE
	@Test
	public void testG39378FE() {
		String glycoct = "RES\n"
				+ "1b:x-lara-HEX-1:5|2:d|6:d\n"
				+ "2s:methyl\n"
				+ "3b:a-lara-HEX-1:5|2:d|6:d\n"
				+ "4s:methyl\n"
				+ "5b:a-lara-HEX-1:5|2:d|6:d\n"
				+ "6s:methyl\n"
				+ "7b:a-lara-HEX-1:5|2:d|6:d\n"
				+ "8s:methyl\n"
				+ "9s:acetyl\n"
				+ "LIN\n"
				+ "1:1o(3+1)2n\n"
				+ "2:1o(4+1)3d\n"
				+ "3:3o(3+1)4n\n"
				+ "4:3o(4+1)5d\n"
				+ "5:5o(3+1)6n\n"
				+ "6:5o(4+1)7d\n"
				+ "7:7o(3+1)8n\n"
				+ "8:7o(4+1)9n\n";
String wurcs = 
				"WURCS=2.0/3,4,3/[ad211m-1x_1-5_3*OC][ad211m-1a_1-5_3*OC][ad211m-1a_1-5_3*OC_4*OCC/3=O]/1-2-2-3/a4-b1_b4-c1_c4-d1";
		
		try {
			check(wurcs, glycoct);
			testG80502ZO();
		} catch (ConvertException e) {
			logger.debug(e.getMessage());
		}
	}
	
	// G24182DO
	@Test
	public void testG24182DO() {
		String glycoct = "RES\n"
				+ "1b:x-HEX-x:x\n"
				+ "2s:n-acetyl\n"
				+ "3b:x-HEX-x:x\n"
				+ "4s:n-acetyl\n"
				+ "5b:x-HEX-x:x\n"
				+ "6s:n-acetyl\n"
				+ "7b:x-HEX-x:x\n"
				+ "8s:n-acetyl\n"
				+ "9b:x-HEX-1:5\n"
				+ "10b:x-HEX-1:5\n"
				+ "11b:x-HEX-1:5\n"
				+ "12b:x-HEX-1:5\n"
				+ "13b:x-HEX-1:5\n"
				+ "14b:x-HEX-1:5|0:d\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:3d(2+1)4n\n"
				+ "3:5d(2+1)6n\n"
				+ "4:7d(2+1)8n\n";
String wurcs = 
				"WURCS=2.0/3,10,0+/[uxxxxh_2*NCC/3=O][axxxxh-1x_1-5_?*][axxxxh-1x_1-5]/1-1-1-1-2-3-3-3-3-3/";
		
		try {
			check(wurcs, glycoct);
		} catch (ConvertException e) {
			logger.debug(e.getMessage());
		}
	}
	
	// G49507PE
	@Test
	public void testG35898DT() {
		String glycoct = "RES\n"
				+ "1b:a-dgal-HEX-1:5\n"
				+ "2s:n-acetyl\n"
				+ "3b:b-dgal-HEX-1:5\n"
				+ "4b:b-dglc-HEX-1:5\n"
				+ "5s:n-acetyl\n"
				+ "6b:b-dgal-HEX-1:5\n"
				+ "7b:a-lgal-HEX-1:5|6:d\n"
				+ "8b:b-dglc-HEX-1:5\n"
				+ "9s:n-acetyl\n"
				+ "10b:b-dglc-HEX-1:5\n"
				+ "11s:n-acetyl\n"
				+ "12b:b-dgal-HEX-1:5\n"
				+ "13b:a-lgal-HEX-1:5|6:d\n"
				+ "LIN\n"
				+ "1:1d(2+1)2n\n"
				+ "2:1o(3+1)3d\n"
				+ "3:3o(3+1)4d\n"
				+ "4:4d(2+1)5n\n"
				+ "5:4o(4+1)6d\n"
				+ "6:6o(2+1)7d\n"
				+ "7:3o(6+1)8d\n"
				+ "8:8d(2+1)9n\n"
				+ "9:1o(6+1)10d\n"
				+ "10:10d(2+1)11n\n"
				+ "11:10o(4+1)12d\n"
				+ "12:12o(2+1)13d\n";
String wurcs = 				"WURCS=2.0/4,9,8/[a2112h-1a_1-5_2*NCC/3=O][a2112h-1b_1-5][a2122h-1b_1-5_2*NCC/3=O][a1221m-1a_1-5]/1-2-3-2-4-3-3-2-4/a3-b1_a6-g1_b3-c1_b6-f1_c4-d1_d2-e1_g4-h1_h2-i1";
		
		try {
			check(wurcs, glycoct);
		} catch (ConvertException e) {
			logger.debug(e.getMessage());
		}
	}

	
	// G49507PE
	@Test
	public void testG49507PE() {
		String glycoct = "RES\n1b:x-dgro-dgal-NON-x:x|1:a|2:keto|3:d";
String wurcs = 				"WURCS=2.0/1,1,0/[AUd21122h]/1/";
		
		try {
			check(wurcs, glycoct);
		} catch (ConvertException e) {
			logger.debug(e.getMessage());
		}
	}
	
	@Test
	public void testG00031MO() throws ConvertException {
		String G00031MO = "RES\\n" + 
				"1b:a-dgal-HEX-1:5\\n" + 
							"2s:n-acetyl\\n" + 
							"3b:b-dgal-HEX-1:5\\n" + 
							"LIN\\n" + 
							"1:1d(2+1)2n\\n" + 
							"2:1o(3+1)3d";
String wurcs = 				"WURCS=2.0/2,2,1/[a2112h-1a_1-5_2*NCC/3=O][a2112h-1b_1-5]/1-2/a3-b1";
		
		check(wurcs, G00031MO);
	}
	
	@Test
	public void testG64539CK() throws ConvertException {
		String G64539CK = "RES\n"
				+ "1b:o-llyx-PEN-0:0|1:aldi";
String wurcs = 				"WURCS=2.0/1,1,0/[h221h]/1/";
		
		check(wurcs, G64539CK);
	}	
	
	
	
	@Bean
	public GlyConvert getConverter() {
		return new GlycoctToWurcsConverter();
	}
	
	private void check(String wurcs, String glycoct) throws ConvertException {
		logger.debug("glycoct:>" + glycoct + "<");
		logger.debug("wurcs:>" + wurcs + "<");
		String result = converter.convert(glycoct);
		logger.debug("result:>" + result + "<");
		assertEquals(wurcs, result);
	}
	public void testct4() throws Exception {
		String result = converter.convert(glycoct3);
		assertEquals(wurcs3, result);
	}

}
