/**
 * 
 */
package org.glycoinfo.convert.glycoct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.ConvertTest;
import org.glycoinfo.convert.GlyConvert;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author aoki
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GlycoctToWurcsToIupacToGlycoctTest.class)
@Configuration
public class GlycoctToWurcsToIupacToGlycoctTest extends ConvertTest {

	Log logger = LogFactory.getLog(GlycoctToWurcsToIupacToGlycoctTest.class);
	
	@Bean
	public GlyConvert getConverter() {
		return new GlycoctToWurcsToIupacToGlycoCTCheckConverter();
	}
}
