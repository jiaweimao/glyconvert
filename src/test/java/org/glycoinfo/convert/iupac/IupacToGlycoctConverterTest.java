/**
 * 
 */
package org.glycoinfo.convert.iupac;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glycoinfo.convert.ConvertTest;
import org.glycoinfo.convert.GlyConvert;
import org.glycoinfo.convert.iupac.IupacToGlycoctMFConverter;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author aoki
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = IupacToGlycoctConverterTest.class)
@Configuration
public class IupacToGlycoctConverterTest extends ConvertTest {

	Log logger = LogFactory.getLog(IupacToGlycoctConverterTest.class);
	
	@Bean
	public GlyConvert getConverter() {
		return new IupacCondensedToGlycoctConverter();
	}
}
